var fs = require('fs'),
    http = require('http'),
    url = require('url'),
    config = {
      ipaddress: process.env.OPENSHIFT_INTERNAL_IP || '127.0.0.1',
      port: process.env.OPENSHIFT_INTERNAL_PORT || 8080
    };

http.createServer(function(req, res) {
  var basename = url.parse(req.url).pathname.replace(/.*\//g, '');

  var filename = './public/' + basename;

  if (basename.length > 3 && fs.existsSync(filename)) {
    if (basename.match(/\.lng$/) &&
        (req.headers['if-none-match'] || req.headers['if-modified-since'])) {
      res.writeHead(304);
      res.end();
    } else {
      fs.readFile(filename, 'binary', function(err, file) {
        if (err) {
          res.writeHead(500, {'Content-Type': 'text/plain'});
          res.write(err + '\n');
          res.end();
          return;
        }

        var mime = 'application/octet-stream';

        res.writeHead(200, {
          'Content-Type': mime,
          'Expires': new Date(new Date().getTime() + 1e9).toUTCString(),
          'Cache-Control': 'public; max-age=1036800',
          'Last-Modified': 'Thu, 09 Apr 2009 06:44:34 GMT'
        });
        res.write(file, 'binary');
        res.end();
      });
    }
    return;
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end('Not found');
  }
}).listen(config.port, config.ipaddress);

// vim: sw=2 ts=2 et
